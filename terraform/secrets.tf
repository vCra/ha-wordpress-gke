resource "random_password" "wordpress-password" {
  length  = 16
  special = true
}
resource "random_password" "mariadb-password" {
  length  = 16
  special = true
}
resource "random_password" "mariadb-root-password" {
  length  = 16
  special = true
}
resource "random_password" "mariadb-backup-password" {
  length  = 16
  special = true
}



resource "kubernetes_secret" "wordpress-password" {
  lifecycle {
    ignore_changes = [
      data,
    ]
  }
  metadata {
    name      = "wordpress-passwords"
    namespace = kubernetes_namespace.wordpress.metadata.0.name
  }
  data = {
    "wordpress-password" : random_password.wordpress-password.result
  }
}

resource "kubernetes_secret" "wordpress-mariadb" {
  lifecycle {
    ignore_changes = [
      data,
    ]
  }
  metadata {
    name      = "wordpress-mariadb-passwords"
    namespace = kubernetes_namespace.wordpress.metadata.0.name
  }
  data = {
    "mariadb-password" : random_password.mariadb-password.result
    "mariadb-galera-mariabackup-password" : random_password.mariadb-backup-password.result
    "mariadb-root-password" : random_password.mariadb-root-password.result
  }
}
