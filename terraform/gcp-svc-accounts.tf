resource "google_service_account" "gke" {
  account_id   = "gke-svc"
  display_name = "Kubernetes Service Account"
}

# Minimal roles needed for GKE - https://cloud.google.com/kubernetes-engine/docs/how-to/access-scopes#minimal
# TODO: better support multile roles
resource "google_project_iam_binding" "gke-metric" {
  project = var.gcp-project
  role    = "roles/monitoring.metricWriter"

  members = [
    "serviceAccount:${google_service_account.gke.email}"
  ]
}
resource "google_project_iam_binding" "gke-monitoring" {
  project = var.gcp-project
  role    = "roles/monitoring.viewer"

  members = [
    "serviceAccount:${google_service_account.gke.email}"
  ]
}
resource "google_project_iam_binding" "gke-logs" {
  project = var.gcp-project
  role    = "roles/logging.logWriter"

  members = [
    "serviceAccount:${google_service_account.gke.email}"
  ]
}
