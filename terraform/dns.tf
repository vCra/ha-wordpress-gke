data "cloudflare_zones" "root-domain" {
  filter {
    name = var.cloudflare_zone
  }
}

resource "cloudflare_record" "wordpress-record" {
  zone_id = data.cloudflare_zones.root-domain.zones.0.id
  name    = var.subdomain
  value   = kubernetes_ingress.wordpress-ingress.status.0.load_balancer.0.ingress.0.ip
  type    = "A"
  ttl     = 1 # Must be set to 1 when proxying through cloudflare
  proxied = true

}