resource "google_container_cluster" "wordpress-cluster" {
  name     = "wordpress"
  location = var.gcp-region

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count       = 1
  ip_allocation_policy {
    # Creates a VPC-native cluster, which allows for VPC-native traffic routing
    # https://cloud.google.com/kubernetes-engine/docs/how-to/alias-ips
    cluster_ipv4_cidr_block  = ""
    services_ipv4_cidr_block = ""
  }
}


resource "google_container_node_pool" "pool-1" {
  name       = "gke-workers"
  location   = var.gcp-region
  node_count = 1 # per zone - * by 3 for total nodes in pool
  cluster    = google_container_cluster.wordpress-cluster.name
  autoscaling {
    max_node_count = 6
    min_node_count = 1
  }
  node_config {
    preemptible  = true
    machine_type = "e2-medium"
    disk_size_gb = "10"
    image_type   = "cos_containerd"

    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    service_account = google_service_account.gke.email
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"

    ]
  }
}
