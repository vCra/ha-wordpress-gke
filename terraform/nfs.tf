resource "helm_release" "nfs-provisioner" {
  chart      = "nfs-server-provisioner"
  name       = "nfs-provisioner"
  repository = "https://kvaps.github.io/charts"
  values = [yamlencode({
    persistence = {
      enabled      = true
      storageClass = "standard-rwo"
      size         = "20Gi"
    }
  })]
}

