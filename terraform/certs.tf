### Cloudflare origin cert
# This only secures traffic between the cluster and cloudflare - any other clients will see this cert as untrusted

resource "tls_private_key" "wordpress_private" {
  algorithm = "RSA"
}
resource "tls_cert_request" "wordpress_csr" {
  key_algorithm   = tls_private_key.wordpress_private.algorithm
  private_key_pem = tls_private_key.wordpress_private.private_key_pem

  subject {
    common_name  = "${var.subdomain}.${var.cloudflare_zone}"
    organization = "${var.cloudflare_zone} and Co."
  }
}

resource "cloudflare_origin_ca_certificate" "wordpress_crt" {

  csr                = tls_cert_request.wordpress_csr.cert_request_pem
  hostnames          = ["${var.subdomain}.${var.cloudflare_zone}"]
  request_type       = "origin-rsa"
  requested_validity = 365 # 15 years should do
}

resource "kubernetes_secret" "origin-tls-cert" {
  metadata {
    name      = "wp-origin-tls"
    namespace = kubernetes_namespace.wordpress.metadata.0.name
  }
  type = "kubernetes.io/tls"
  data = {
    "tls.crt" = cloudflare_origin_ca_certificate.wordpress_crt.certificate
    "tls.key" = tls_private_key.wordpress_private.private_key_pem
  }
}