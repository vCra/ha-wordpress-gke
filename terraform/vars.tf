variable "gcp-project" {
  description = "GCP Project ID"
}
variable "gcp-region" {
  description = "GCP Region"
}

variable "cloudflare_api_token" {
  description = "An API Key for your cloudflare account"
}
variable "cloudflare_user_service_key" {
  description = "The User service key from cloudflare"
}

variable "cloudflare_zone" {
  description = "Your domain zone"
  default     = "vcra.io"
}
variable "subdomain" {
  default = "wordpress"
}