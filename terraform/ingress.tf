resource "kubernetes_ingress" "wordpress-ingress" {
  metadata {
    name      = var.subdomain
    namespace = kubernetes_namespace.wordpress.metadata.0.name
    annotations = {
      "cloud.google.com/load-balancer-type" = "External"
      "kubernetes.io/ingress.class" : "gce"
    }
  }
  spec {
    rule {
      http {
        path {
          backend {
            service_name = "wordpress"
            service_port = "http"
          }
        }

      }
      host = "${var.subdomain}.${var.cloudflare_zone}"
    }
    tls {
      hosts = [
        "${var.subdomain}.${var.cloudflare_zone}"
      ]
      secret_name = kubernetes_secret.origin-tls-cert.metadata.0.name
    }
  }
  wait_for_load_balancer = true
}

resource "kubernetes_manifest" "ingress-backend" {
  provider = kubernetes-alpha

  manifest = {
    apiVersion = "cloud.google.com/v1"
    kind       = "BackendConfig"
    metadata = {
      name      = "wordpress-backend-config"
      namespace = kubernetes_namespace.wordpress.metadata.0.name
    }
    spec = {
      sessionAffinity = {
        affinityType : "GENERATED_COOKIE"
        affinityCookieTtlSec : 3600
      }
    }
  }
}
