terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 2.0"
    }
  }
}

provider "google" {
  project = var.gcp-project
  region  = var.gcp-region
}

provider "cloudflare" {
  api_token            = var.cloudflare_api_token
  api_user_service_key = var.cloudflare_user_service_key
}

# Uses gcloud token
data "google_client_config" "gcloud" {}

provider "kubernetes" {
  host  = "https://${google_container_cluster.wordpress-cluster.endpoint}"
  token = data.google_client_config.gcloud.access_token
  cluster_ca_certificate = base64decode(
    google_container_cluster.wordpress-cluster.master_auth[0].cluster_ca_certificate,
  )
}

provider "helm" { # We have to copy config as we're unable to access provider attributes
  kubernetes {
    host  = "https://${google_container_cluster.wordpress-cluster.endpoint}"
    token = data.google_client_config.gcloud.access_token
    cluster_ca_certificate = base64decode(
      google_container_cluster.wordpress-cluster.master_auth[0].cluster_ca_certificate,
    )
  }
}

provider "kubernetes-alpha" {
  host  = "https://${google_container_cluster.wordpress-cluster.endpoint}"
  token = data.google_client_config.gcloud.access_token
  cluster_ca_certificate = base64decode(
    google_container_cluster.wordpress-cluster.master_auth[0].cluster_ca_certificate,
  )
}