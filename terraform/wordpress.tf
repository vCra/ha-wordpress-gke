resource "kubernetes_namespace" "wordpress" {
  metadata {
    name = "wordpress"
  }
}
resource "helm_release" "wordpress" {
  depends_on = [
    kubernetes_secret.wordpress-mariadb,
    kubernetes_secret.wordpress-password
  ]
  name       = "wordpress"
  namespace  = kubernetes_namespace.wordpress.metadata.0.name
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "wordpress"
  version    = "11.0.3"
  values = [yamlencode({
    replicaCount         = 3
    existingSecret       = kubernetes_secret.wordpress-password.metadata.0.name, # Not used as we manually install WP once
    wordpressSkipInstall = true                                                  # We need to manually install once, rather than on each instance
    externalDatabase = {
      existingSecret = kubernetes_secret.wordpress-mariadb.metadata.0.name,
      host           = "wp-db-mariadb-galera.wordpress.svc.cluster.local"
      port           = 3306
      user           = "wordpress"
      database       = "wordpress"
    }
    mariadb = {
      enabled = false # Don't deploy the wordpress-managed maria-db server
    }
    service = {
      type = "NodePort"
      annotations = {
        "cloud.google.com/backend-config" = "{\"default\": \"wordpress-backend-config\"}"
        //        "cloud.google.com/neg" = "{'ingress': true}"
      }
    }
    persistence = {
      storageClass = "nfs"
      accessModes  = ["ReadWriteMany"]
    }

    # FIXME: Looks like the auto-configure of memcached breaks the wordpress settings permissions - leave out for now
    //    memcached = {
    //      enabled = true
    //    }
    //    wordpressConfigureCache = true # Install the cache plugin automagically
    //    volumePermissions = {
    //      enabled = true # Permissions changed somehow?
    //    }
  })]
}