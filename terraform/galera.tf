# MariaDB Galera Cluster (Read Multi-resource "kubernetes_namespace" "wordpress" {
resource "helm_release" "galera" {
  depends_on = [
    kubernetes_secret.wordpress-mariadb,
  ]
  name       = "wp-db"
  namespace  = kubernetes_namespace.wordpress.metadata.0.name
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "mariadb-galera"
  version    = "5.8.0"
  values = [yamlencode({
    existingSecret = kubernetes_secret.wordpress-mariadb.metadata.0.name
    db = {
      name = "wordpress"
      user = "wordpress"
    }
  })]

}