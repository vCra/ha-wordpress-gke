# HA Wordpress in GKE

Quickly deploys a GKE cluster, with HA Wordpress and MariaDB

## Requirements

* Terraform > 0.14.8 (tested on 0.15.3)
* gcloud cli
* A working GCP account and empty project - ensure you log in with `gcloud auth login`
* A CloudFlare account with a DNS Zone configured as well as:
  * An API token, with edit perms for the `Zone.Zone` and `Zone.DNS` scopes
  * A User Service token (Origin CA Key)
* A local copy of this repo

## Getting started

1. Clone this repo
2. Populate terraform/terraform.tfvars with the following
   - `gcp-project` - the project ID of your GCP Project. See how to create one [here](https://developers.google.com/workspace/guides/create-project).
   - `gcp-region` - the region you created your GCP project in. `europe-west2` is a good shout
   - `cloudflare_api_token` - [Create an API Token](https://dash.cloudflare.com/profile/api-tokens) with the `zone.zone` and `zone.dns` scopes
   - `cloudflare_user_service_key` - Copy the value of the "Origin CA Key"
   - `cloudflare_zone` - Enter your cloudflare zone. e.g. `vcra.io`
3. Run `terraform init` to download terraforms provisioners.
4. Run `terraform apply` to create all the required resources

### Initial Wordpress Installation

The GCP check is very picky about any HTTP Response codes it gets back, and only shows healthy on a 200 and not on any redirects.
We need to manually install Wordpress before the pod shows as ready, and we can connect to it via our domain.
We can do this during the initial `terraform apply`, after `helm_release.wordpress` has completed sucessfully

1. Configure your clusters kubeconfig (GKE -> Cluster -> Connect)
2. Get the name of the wordpress pod 
   ```
   kubectl get pods -n wordpress
   NAME                                   READY   STATUS    RESTARTS   AGE
   wordpress-7d97bbf5f4-97nd2             1/1     Running   0          7m28s
   wordpress-memcached-5bf69ccd7d-5dvgj   1/1     Running   0          7m28s
   wp-db-mariadb-galera-0                 1/1     Running   0          32m
   wp-db-mariadb-galera-1                 1/1     Running   0          30m
   wp-db-mariadb-galera-2                 1/1     Running   0          29m
   ```
3. Port forward the wordpress pod `kubectl port-forward -n wordpress wordpress-7d97bbf5f4-97nd2 8080:8080`
4. Go to `localhost:8080` and fill in the Wordpress installation form (e.g. give your site a Name, and create a username/password)
5. Your ingress should be created as the healthcheck succeeds 

## Features

### Multi-Replica Wordpress

A 3-replica deployment of Wordpress is installed via Helm.
Each pod within the deployment connects to a Single Persistant Volume provided by NFS, and to the Galera cluster via
it's respective service.
Since we use a VPC-native cluster, we don't need to rely on kube-proxy in order to load balance between workloads, but 
instead can use the gcp ingress to directly connect to the pod on each node. This has the advantage of requiring less 
hops between each of the replicas
The ingress is configured with cookie-based session-affinity, so that users are sent to the same pod

### NFS
Since GCP does not support ReadWriteMany PVC's, and only supports block storage for file access (rather than object
 storage) we need to find a way to store WordPress's Shared Data
[nfs-provisioner](https://github.com/kubernetes-sigs/nfs-ganesha-server-and-external-provisioner) is deployed, and
configured to use a single GCE PD as a backing store. The PVC created by Wordpress has it's storageClass set to use 
NFS instead.
Note that this is not a HA instance of NFS

### MariaDB Galera
Galera is a Multi-master distro of MariaDB
Galera is deployed using the Bitnami Helm Chart.
Secrets for MariaDB are generated using `random_password` in `secrets.tf`, and then stored in a k8s secret.
WordPress accesses it via it's service

### Caching & TLS via CloudFlare
Once the GCP Ingress has a public IP, a DNS record is created in CloudFlare 

## Things I learnt

* Google has a CDN. It sounds stupid after the fact, but initially I architected the system so that compute was all in
GKE, and DNS, Certs and caching were in CloudFlare. (GCP also supports DNS and Cert management too 🙃)
* GKE doesn't support  voReadWriteManylumes unless you folk-out for their enterprise NFS offering

## Terraform layout

```
├── certs.tf - Configures the CloudFlare Origin Cert for k8s ingress
├── dns.tf - Configures the CloudFlare proxied DNS record
├── galera.tf - Deployment of the HA MariaDB Galera Cluster
├── gcp-svc-accounts.tf - Configures the Service account used by GKE in order to get/post metrics/logs
├── gke.tf - Creates the GKE cluster
├── ingress.tf - Config for Load Balancer and Service Backend Config
├── nfs.tf - Deployment of nfs-provisioner to allow for ReadWriteMany Volumes
├── providers.tf - Terraform providers
├── secrets.tf - Generates passwords for MariaDB
├── terraform.tfvars
├── vars.tf
└── wordpress.tf - Helm Deployment of Wordpress

```

## Tools used

* Terraform for IaC
* K8s to run everything on
* Git as source control
* Helm for K8s packages

